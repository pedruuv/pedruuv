# 👨‍💻 About Me:
⚡  I'm currently enrolled in a software engineering program, where I'm immersing myself in the intricacies of programming and software design.<br><br>🌱 I’m currently learning Java and the Spring framework. The prospect of diving deep into these technologies fuels my excitement, as it enables me to build robust, scalable applications that can drive transformative changes in the software landscape. My quest involves not only mastering the technical aspects but also understanding how these technologies fit within the broader context of software architecture.<br><br>🔭 My pursuit revolves around securing my first role as a backend developer. I'm eager to channel my skills and enthusiasm into a dynamic team where I can contribute meaningfully, learn extensively, and evolve professionally. My goal is to immerse myself in backend complexities, whether it's optimizing data storage, designing APIs, or ensuring system reliability.<br><br>💬 If you're interested in connecting, discussing software engineering, or sharing insights about the field, feel free to reach out to me on [LinkedIn](https://www.linkedin.com/in/pedro-vitor-gurgel-9794b0284/) or [GitLab](https://gitlab.com/pedruuv). I'm enthusiastic about connecting with fellow students, mentors, and professionals to exchange knowledge and experiences. Let's embark on a journey of learning and growth in the world of software engineering! 💻🚀


## 🌐 Socials:
[![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white)](https://linkedin.com/in/pedro-vitor-gurgel-9794b0284) 
[![GitLab](https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/pedruuv)
[![Hackerrank](https://img.shields.io/badge/-Hackerrank-2EC866?style=for-the-badge&logo=HackerRank&logoColor=white)](https://www.hackerrank.com/impedrovgurgel)
[![Stack Overflow](https://img.shields.io/badge/-Stackoverflow-FE7A16?style=for-the-badge&logo=stack-overflow&logoColor=white)](https://pt.stackoverflow.com/users/323389/pedrovitorgl)

# 💻 Tech Stack:
![C#](https://img.shields.io/badge/c%23-%23239120.svg?style=for-the-badge&logo=c-sharp&logoColor=white) ![Go](https://img.shields.io/badge/go-%2300ADD8.svg?style=for-the-badge&logo=go&logoColor=white) ![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=java&logoColor=white) ![Azure](https://img.shields.io/badge/azure-%230072C6.svg?style=for-the-badge&logo=azure-devops&logoColor=white) ![Oracle](https://img.shields.io/badge/Oracle-F80000?style=for-the-badge&logo=oracle&logoColor=white) ![.Net](https://img.shields.io/badge/.NET-5C2D91?style=for-the-badge&logo=.net&logoColor=white) ![Spring](https://img.shields.io/badge/spring-%236DB33F.svg?style=for-the-badge&logo=spring&logoColor=white) ![MicrosoftSQLServer](https://img.shields.io/badge/Microsoft%20SQL%20Sever-CC2927?style=for-the-badge&logo=microsoft%20sql%20server&logoColor=white) ![MongoDB](https://img.shields.io/badge/MongoDB-%234ea94b.svg?style=for-the-badge&logo=mongodb&logoColor=white) ![Postgres](https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white) ![LINUX](https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black) ![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)
# 📊 GitHub Stats:
![](https://github-readme-stats.vercel.app/api?username=pedruuv&theme=gotham&hide_border=false&include_all_commits=true&count_private=true)<br/>
![](https://github-readme-streak-stats.herokuapp.com/?user=pedruuv&theme=gotham&hide_border=false)<br/>
![](https://github-readme-stats.vercel.app/api/top-langs/?username=pedruuv&theme=gotham&hide_border=false&include_all_commits=true&count_private=true&layout=compact)

### 🔝 Top Contributed Repo
![](https://github-contributor-stats.vercel.app/api?username=pedruuv&limit=5&theme=apprentice&combine_all_yearly_contributions=true)


---
[![](https://visitcount.itsvg.in/api?id=pedruuv&icon=2&color=9)](https://visitcount.itsvg.in)

<!-- Proudly created with GPRM ( https://gprm.itsvg.in ) -->
